import React from "react";

import homeImageTriple1 from "../images/HomeImageTriple1.jpeg";
import homeImageTriple2 from "../images/HomeImageTriple2.jpeg";
import homeImageTriple3 from "../images/HomeImageTriple3.jpg";

const ThreePictureSpread = () => {
  return (
    <div className="container">
      <div className="row">
        <div className="col-xs-12 col-md-4">
          <img src={homeImageTriple1} alt="homeImageTriple 1" width="100%" />
        </div>
        <div className="col-xs-12 col-md-4">
          <img src={homeImageTriple2} alt="homeImageTriple 2" width="100%" />
        </div>
        <div className="col-xs-12 col-md-4">
          <img src={homeImageTriple3} alt="homeImageTriple 3" width="100%" />
        </div>
      </div>
    </div>
  );
};

export default ThreePictureSpread;
