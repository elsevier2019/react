import React from "react";

import FilmTable from "./FilmTable";

import { FILMS as films } from "../../js/cinemaInfo";

const Films = () => {
  // let currentFilms = [];
  // let futureFilms = [];
  // films.forEach(film => {
  //   if (film.filmStatus === "1") {
  //     currentFilms.push(film);
  //   } else if (film.filmStatus === "2") {
  //     futureFilms.push(film);
  //   }
  //   return;
  // });
  return (
    <>
      <FilmTable films={films} title="What's On" />
      {/* {currentFilms.length > 0 ? (
        <FilmTable films={currentFilms} title="What's On" />
      ) : null}
      {futureFilms.length > 0 ? (
        <FilmTable films={futureFilms} title="ComingSoon" />
      ) : null} */}
    </>
  );
};

export default Films;
