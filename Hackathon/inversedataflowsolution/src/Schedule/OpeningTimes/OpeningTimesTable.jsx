import React from "react";

import OpeningTimesRow from "./OpeningTimesRow";

import { OPENINGS as openingTimes } from "../../js/cinemaInfo";

const OpeningTimesTable = () => {
  const openingTimesRows = openingTimes.map(openingTime => (
    <OpeningTimesRow openingTime={openingTime} key={openingTime._id} />
  ));

  return (
    <div className="table-responsive">
      <table className="table table-borderless">
        <thead>
          <tr>
            <th></th>
            <th className="td-left-align">Times</th>
          </tr>
        </thead>
        <tbody>{openingTimesRows}</tbody>
      </table>
    </div>
  );
};

export default OpeningTimesTable;
