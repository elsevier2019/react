import React from "react";

import Films from "./Films/Films";
import OpeningTimes from "./OpeningTimes/OpeningTimes";

import scheduleBanner from "../images/ScheduleBanner.jpg";

const Schedule = () => {
  return (
    <>
      <div className="container">
        <img src={scheduleBanner} alt="Schedule Banner" width="100%" />
      </div>
      <OpeningTimes />
      <Films />
    </>
  );
};

export default Schedule;
