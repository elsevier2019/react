import React from 'react';
import { Route, Switch } from 'react-router-dom';

import Home from '../Home/Home';
import Schedule from '../Schedule/Schedule';
import SignUp from '../SignUp/SignUp';
import Film from '../Schedule/Films/Film';

const Content = () => {
    return (
      <main>
        <Switch>
          <Route path="/" exact component={Home} />
          <Route path="/signup" component={SignUp} />
          <Route path="/film/:id" component={Film} />
          <Route path="/schedule" component={Schedule} />
        </Switch>
      </main>
    );
}

export default Content;