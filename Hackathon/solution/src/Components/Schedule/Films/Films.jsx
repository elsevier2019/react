import React from "react";

import FilmsTable from "./FilmsTable";

const Films = props => {
  if (
    (props.films.length === 1 && typeof props.films[0] === "string") ||
    props.films.length === 0
  ) {
    return (
      <div className="container">
        <h1>{props.heading}</h1>
        <p>{`There are no ${props.heading} films to display at the moment`}</p>
      </div>
    );
  } else {
    return (
      <div className="container">
        <h1>{props.heading}</h1>
        <FilmsTable films={props.films} status={props.status} />
      </div>
    );
  }
};

export default Films;
