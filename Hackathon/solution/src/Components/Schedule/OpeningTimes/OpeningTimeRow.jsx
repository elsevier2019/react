import React from "react";

const OpeningTimesRow = props => {
  return (
    <tr>
      <td className="td-right-align">{props.openingTime.days}</td>
      <td className="td-left-align">{props.openingTime.times}</td>
    </tr>
  );
};

export default OpeningTimesRow;
