import React, { useState, useEffect } from "react";
import Loader from "react-loader-spinner";
import axios from "axios";
import ImageContainer from "../Content/ImageContainer";
import { IMAGES } from "../../js/constants/images";
// import { films } from '../../js/constants/films';

import OpeningTimes from "./OpeningTimes/OpeningTimes";

import Films from "./Films/Films";

import { GETALLFILMS } from "../../js/constants/urls";

const Schedule = () => {
  const [currentFilms, setCurrentFilms] = useState([]);
  const [futureFilms, setFutureFilms] = useState([]);
  const [loading, setLoading] = useState(true);
  const current = { status: "1", heading: `What's On` };
  const future = { status: "2", heading: `Coming Soon` };

  useEffect(() => {
    const getFilms = async () => {
      try {
        const response = await axios(`${GETALLFILMS}`);
        const fetchedFilms = await response.data;
        let fetchedCurrentFilms = currentFilms;
        let fetchedFutureFilms = futureFilms;
        fetchedFilms.forEach(film => {
          if (film.filmStatus === "1") {
            fetchedCurrentFilms.push(film);
          } else if (film.filmStatus === "2") {
            fetchedFutureFilms.push(film);
          } else {
            throw new Error("Error with film status");
          }
        });
        if (fetchedCurrentFilms.length > 0)
          setCurrentFilms(fetchedCurrentFilms);
        if (fetchedFutureFilms.length > 0) setFutureFilms(fetchedFutureFilms);
      } catch (e) {
        setCurrentFilms([e.message]);
        setFutureFilms([e.message]);
      }
      setLoading(false);
    };
    getFilms();
  }, []);

  return (
    <>
      <div className="container">
        <ImageContainer image={IMAGES.scheduleBanner} width="100%" />
      </div>
      <div className="container">
        <OpeningTimes />
      </div>
      <div className="container">
        {!loading ? (
          <>
            <Films
              films={currentFilms}
              heading={current.heading}
              status={current.status}
            />
            <Films
              films={futureFilms}
              heading={future.heading}
              status={future.status}
            />
          </>
        ) : (
          <Loader type="Watch" color="rgb(0, 0, 255)" height={30} width={30} />
        )}
      </div>
    </>
  );
};

export default Schedule;
