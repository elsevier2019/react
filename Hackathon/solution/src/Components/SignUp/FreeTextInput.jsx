import React from 'react';

const FreeTextInput = props => {
    const validityCSSClassname = !props.value.touched || props.value.valid ? `valid` : `invalid`;

    return (
        <div className="form-group row">
            <label htmlFor={props.fieldName} className="col-sm-2 col-form-label">{props.fieldDisplay}{props.required ? `*` : null}</label>
            <div className="col-sm-6">
                <input 
                    type={props.type} 
                    className={`${validityCSSClassname} form-control`} 
                    required={props.required} 
                    name={props.fieldName} 
                    placeholder={props.fieldDisplay}
                    value={props.value.value} 
                    onChange={props.onchange} 
                />
            </div>
            {/* Hidden true when:
                valid = false AND touched = false
                OR
                valid = true AND touched = true    
            */}
            <small className="col-sm-2" hidden={(!props.value.touched) || (props.value.valid)}>Please enter your {props.fieldDisplay.toLowerCase()}</small>
        </div>
    );
}

export default FreeTextInput;