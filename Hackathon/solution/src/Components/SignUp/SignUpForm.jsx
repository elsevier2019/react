import React, { useState } from "react";

import TitleOption from "./TitleOption";
import FreeTextInput from "./FreeTextInput";
import Checkboxes from "./Checkboxes";
import SubmitButton from "./SubmitButton";
import { POSTSIGNUP } from "../../js/constants/urls";
import generateUserId from "../../js/generateId";

const SignUpForm = () => {
  const [title, setTitle] = useState({
    value: ``,
    valid: false,
    touched: false
  });
  const [firstName, setFirstName] = useState({ value: ``, valid: false });
  const [lastName, setLastName] = useState({ value: ``, valid: false });
  const [email, setEmail] = useState({ value: ``, valid: false });
  const [phoneNumber, setPhoneNumber] = useState({ value: ``, valid: true });
  const [dob, setDob] = useState(``);
  const [gender, setGender] = useState(``);

  const validateString = input => {
    return /^([A-Za-z'-]{2,})$/.test(input);
  };

  const validateEmail = input => {
    return /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(
      input
    );
  };

  const validatePhoneNumber = input => {
    return /^(\+44\s?7\d{3}|\(?07\d{3}\)?)\s?\d{3}\s?\d{3}$/.test(input);
  };

  const sendData = async person => {
    const response = await fetch(POSTSIGNUP, {
      method: `POST`,
      mode: `cors`,
      headers: {
        "Content-Type": "application/json"
      },
      body: JSON.stringify(person)
    });
    if (response.status === 200 || response.status === 201) {
      alert(`Data sent successfully`);
    } else {
      alert(`There was an error sending the data`);
    }
  };

  const handleChange = event => {
    switch (event.target.name) {
      case `title`:
        setTitle({ value: event.target.value, valid: true, touched: true });
        break;
      case `firstName`:
        if (validateString(event.target.value)) {
          setFirstName({
            value: event.target.value,
            valid: true,
            touched: true
          });
        } else {
          setFirstName({
            value: event.target.value,
            valid: false,
            touched: true
          });
        }
        break;
      case `lastName`:
        if (validateString(event.target.value)) {
          setLastName({
            value: event.target.value,
            valid: true,
            touched: true
          });
        } else {
          setLastName({
            value: event.target.value,
            valid: false,
            touched: true
          });
        }
        break;
      case `email`:
        if (validateEmail(event.target.value)) {
          setEmail({ value: event.target.value, valid: true, touched: true });
        } else {
          setEmail({ value: event.target.value, valid: false, touched: true });
        }
        break;
      case `phoneNumber`:
        if (validatePhoneNumber(event.target.value)) {
          setPhoneNumber({
            value: event.target.value,
            valid: true,
            touched: true
          });
        } else {
          setPhoneNumber({
            value: event.target.value,
            valid: false,
            touched: true
          });
        }
        break;
      case `dob`:
        const dobForSubmit = new Date(event.target.value).toISOString();
        setDob(dobForSubmit);
        break;
      case `gender`:
        setGender(event.target.value);
        break;
      default:
        break;
    }
  };

  const handleSubmit = event => {
    event.preventDefault();
    if (
      title.valid &&
      firstName.valid &&
      lastName.valid &&
      email.valid &&
      (phoneNumber.valid || phoneNumber.value === ``)
    ) {
      const _id = generateUserId();
      const person = {
        _id,
        title: title.value,
        firstName: firstName.value,
        lastName: lastName.value,
        email: email.value,
        phoneNumber: phoneNumber.value,
        dob,
        gender
      };
      sendData(person);
    } else {
      alert(`There is invalid data`);
    }
  };

  let submitStatus =
    title.valid && firstName.valid && lastName.valid && email.valid;

  return (
    <div className="container">
      <form noValidate className="needs-validation">
        <TitleOption onchange={handleChange} value={title} />
        <FreeTextInput
          fieldName="firstName"
          type="text"
          onchange={handleChange}
          value={firstName}
          fieldDisplay="First Name"
          required={true}
        />
        <FreeTextInput
          fieldName="lastName"
          type="text"
          onchange={handleChange}
          value={lastName}
          fieldDisplay="Last Name"
          required={true}
        />
        <FreeTextInput
          fieldName="email"
          type="email"
          onchange={handleChange}
          value={email}
          fieldDisplay="Email"
          required={true}
        />
        <FreeTextInput
          fieldName="phoneNumber"
          type="text"
          onchange={handleChange}
          value={phoneNumber}
          fieldDisplay="Phone Number"
          required={false}
        />
        <FreeTextInput
          fieldName="dob"
          type="date"
          onchange={handleChange}
          value={dob}
          fieldDisplay="Date of Birth"
          required={false}
        />
        <Checkboxes onchange={handleChange} value={gender} />
        <SubmitButton handleSubmit={handleSubmit} status={submitStatus} />
      </form>
    </div>
  );
};

export default SignUpForm;
