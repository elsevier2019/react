import homeBanner from "../../images/HomeBanner.jpeg";
import homeImageTriple1 from "../../images/HomeImageTriple1.jpeg";
import homeImageTriple2 from "../../images/HomeImageTriple2.jpeg";
import homeImageTriple3 from "../../images/HomeImageTriple3.jpg";
import scheduleBanner from "../../images/ScheduleBanner.jpg";
import signupBanner from "../../images/SignupBanner.jpg";
import kot from "../../images/KingOfThieves.jpg";
import predator from "../../images/ThePredator.jpg";
import hwaciiw from "../../images/TheHouse.jpg";
import noImage from "../../images/No_image_available.svg";

export const IMAGES = {
  homeBanner: { src: homeBanner, alt: `QA Cinemas` },
  homeImageTriple1: { src: homeImageTriple1, alt: `QA Cinema` },
  homeImageTriple2: { src: homeImageTriple2, alt: `QA Cinema Lounge` },
  homeImageTriple3: { src: homeImageTriple3, alt: `QA Reception Area` },
  scheduleBanner: { src: scheduleBanner, alt: `QA Schedule` },
  signupBanner: { src: signupBanner, alt: `QA Sign Up` }
};

export const FILMIMAGES = {
  "King of Thieves": {
    src: kot,
    alt: `King of Thieves`
  },
  "The Predator": {
    src: predator,
    alt: `The Predator`
  },
  "The House with a Clock in its Walls": {
    src: hwaciiw,
    alt: `The House with a Clock in its Walls`
  },
  "No Image": {
    src: noImage,
    alt: `Image not available`
  }
};
