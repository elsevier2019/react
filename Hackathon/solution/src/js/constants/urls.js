const baseUrl = `http://localhost:4000`;
export const GETALLFILMS = `${baseUrl}/films`;
export const GETOPENINGS = `${baseUrl}/openingTimes`;
export const GETSINGLEFILM = `${baseUrl}/films/`;
export const POSTSIGNUP = `${baseUrl}/signup`;
