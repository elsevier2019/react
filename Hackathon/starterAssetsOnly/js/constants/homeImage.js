export const HOMEIMAGES = [
  { src: `images/HomeBanner.jpeg`, alt: `QA Cinemas HomePageBanner` },
  { src: `images/HomeImageTriple1.jpeg`, alt: `QA Cinemas` },
  { src: `images/HomeImageTriple2.jpeg`, alt: `QA Cinemas` },
  { src: `images/HomeImageTriple3.jpeg`, alt: `QA Cinemas` }
];
