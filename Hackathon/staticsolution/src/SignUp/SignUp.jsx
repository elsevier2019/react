import React from "react";

import SignUpForm from "./SignUpForm";

import signupBanner from "../images/SignupBanner.jpg";

const SignUp = () => {
  return (
    <>
      <div className="container">
        <img src={signupBanner} alt="Sign Up Banner" width="100%" />
      </div>
      <SignUpForm />
    </>
  );
};

export default SignUp;
