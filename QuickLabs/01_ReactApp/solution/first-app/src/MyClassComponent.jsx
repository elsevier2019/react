import React, { Component } from 'react';

export class MyClassComponent extends Component {
    render() {
        return (
            <>
                <h2>I am a class component</h2>
                <p>I work in almost the same way as Function components</p>
            </>
        );
    }
};

export default MyClassComponent;
