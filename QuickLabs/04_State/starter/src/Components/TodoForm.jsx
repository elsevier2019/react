import React from 'react';

import DateCreated from './DateCreated';

const TodoForm = props => {
    return (
        <form>
            <div className="form-group">
                <label htmlFor="todoDescription">Description:</label>
                <input type="text" name="todoDescription" placeholder="Todo Description" className="form-control" />
            </div>
            <div className="form-group">
                <label htmlFor="todoDateCreated">Created on:</label>
                <DateCreated dateCreated={props.todo ? props.todo.todoDateCreated : null} />
            </div>
            <div className="form-group">
                <input type="submit" value="Submit" className="btn btn-primary" />
            </div>
        </form >
    );
};

export default TodoForm;
