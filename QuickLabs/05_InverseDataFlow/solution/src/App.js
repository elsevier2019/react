import React, { useState } from 'react';
import "bootstrap/dist/css/bootstrap.min.css";

import Header from './Components/Header';
import Footer from './Components/Footer';
import AllTodos from './Components/AllTodos';
import AddTodo from './Components/AddTodo';
import sampleTodos from './sampleTodos.json';

function App() {
  const [todos, setTodos] = useState(sampleTodos);

  const submitNewTodo = todo => {
    const updatedTodos = [...todos, todo];
    setTodos(updatedTodos);
  };

  return (
    <div className="container">
      <Header />
      <div className="container">
        <AllTodos allTodos={todos} />
        <AddTodo submitNewTodo={submitNewTodo} />
      </div>
      <Footer />
    </div>
  );
}

export default App;
