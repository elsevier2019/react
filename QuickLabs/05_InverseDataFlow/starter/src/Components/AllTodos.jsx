import React from 'react';

import './css/AllTodos.css';


import Todo from './Todo';

const AllTodos = props => {

    const todos = props.todos.map(currentTodo =>
        <Todo todo={currentTodo} key={currentTodo._id} />);

    return (
        <div className="container">
            <h3>Todos List</h3>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>Description</th>
                        <th>Date Created</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    {todos}
                </tbody>
            </table>
        </div>
    );
};

export default AllTodos;
