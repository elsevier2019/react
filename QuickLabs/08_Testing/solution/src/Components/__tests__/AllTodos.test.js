import React from "react";
import { shallow } from "enzyme";
import toJson from "enzyme-to-json";

import AllTodos from "../AllTodos";
import Todo from "../Todo";

import mockTodos from "../../sampleTodos.json";

describe(`AllTodos Component Tests`, () => {
  it(`should render without errors when appropriate props are supplied`, () => {
    const props = { allTodos: [] };
    const testComponent = shallow(<AllTodos {...props} />);
    expect(toJson(testComponent)).toMatchSnapshot();
  });

  it("should render with the error message in the table when allTodos is an empty array", () => {
    const props = { allTodos: [] };
    const testComponent = shallow(<AllTodos {...props} />);
    expect(toJson(testComponent)).toMatchSnapshot();
    const text = testComponent.find('td[colSpan="3"]').text();
    expect(text).toEqual("There has been an error retrieving the todos");
  });

  it("should render with the loading message in the table when the loading prop is true", () => {
    const props = { allTodos: [], loading: true };
    const testComponent = shallow(<AllTodos {...props} />);
    expect(toJson(testComponent)).toMatchSnapshot();
    const text = testComponent.find('td[colSpan="3"]').text();
    expect(text).toEqual("Please wait - retrieving the todos");
  });

  it("should render 4 Todo components when mockTodos are passed as allTodos", () => {
    const props = { allTodos: mockTodos, loading: false };
    const testComponent = shallow(<AllTodos {...props} />);
    expect(toJson(testComponent)).toMatchSnapshot();
    expect(testComponent.find(Todo).length).toBe(4);
  });
});
