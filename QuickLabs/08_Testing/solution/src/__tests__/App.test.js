import React from "react";
import { shallow } from "enzyme";

import App from "../App";
import AllTodos from "../Components/AllTodos";

describe("<App />", () => {
  it("renders without crashing", () => {
    const testComponent = shallow(<App />);
    expect(testComponent).toMatchSnapshot();
  });
});
